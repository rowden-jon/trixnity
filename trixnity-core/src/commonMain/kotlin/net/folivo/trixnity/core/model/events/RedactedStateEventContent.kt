package net.folivo.trixnity.core.model.events

data class RedactedStateEventContent(val eventType: String) : StateEventContent